export class ServiceModel{
    id: string;
    name: string;
    description: string;
    clientId: string;
}

export class ServiceData{
    data: ServiceModel[];
}