export class UserModel{
    firstname: string;
    lastname: string;
    email: string;
    username: string;
    password: string;
    clientId: string;
  }